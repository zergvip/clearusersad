#������ ���������, ��� ������� ������ �������

$global:ExcludedUsers ="1","exam" , "admin"
$global:LocalProfiles=Get-WMIObject -class Win32_UserProfile | Where {(!$_.Special) -and (!$_.Loaded) }



function Get-List{
    foreach ($LocalProfile in $global:LocalProfiles){             
        if (!($global:ExcludedUsers -like $LocalProfile.LocalPath.Replace("C:\Users\",""))){        
                              
            Write-host $LocalProfile.LocalPath -ForegroundColor Green
        }   
    }
}


function Remove-Users{
    foreach ($LocalProfile in $global:LocalProfiles){            
        if (!($global:ExcludedUsers -like $LocalProfile.LocalPath.Replace("C:\Users\",""))){        
                              
            Write-host $LocalProfile.LocalPath, " ������...�������� -ForegroundColor Green
            $LocalProfile | Remove-WmiObject
            Write-host $LocalProfile.LocalPath, "������� ������ -ForegroundColor Magenta
        }    
    }    
}


if($args.count -eq 0){
    Get-List
    Exit
}


if($args[0]  -eq "-l" ){
   Get-List
   Exit 
}

if($args[0] -eq "-r" ){
   Remove-Users
   Exit 
}
